;;; -*- no-byte-compile: t -*-
(define-package "goo" "0.155" "generic object-orientator (Emacs support)")

;;;###autoload
(setq auto-mode-alist (cons '("\\.goo\\'" . goo-mode) auto-mode-alist))
